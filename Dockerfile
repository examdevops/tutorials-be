FROM amazoncorretto:17-alpine3.16

       # Définissez le répertoire de travail à l'intérieur du conteneur
       WORKDIR /app

       # Copiez le contenu du projet tutorials-be dans le conteneur
       COPY . /app

        # Indiquer le port sur lequel l'application écoute
        EXPOSE 8091


        # Spécifiez la commande pour exécuter votre application
        CMD ["java", "-jar", "/app.jar"]